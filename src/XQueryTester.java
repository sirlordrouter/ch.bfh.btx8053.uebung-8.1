import java.io.IOException;

import javax.xml.xquery.XQException;

import org.jdom2.JDOMException;


public class XQueryTester {

	/**
	 * @param args
	 * @throws XQException 
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public static void main(String[] args) throws XQException, JDOMException, IOException {
		// TODO Auto-generated method stub

		BaseXImporter ex = new BaseXImporter();
		
		try {
			String result = ex.executeXQuery(
			"for $country in db:open(\"factbook\",\"factbook.xml\")//country,"+
			"$city in $country//city " +
			"where $city/population/text() > 500000 " +
			"order by number($city/population) descending " +
			"return string-join(" +
			"($country/name/text(), $city/name/text(), $city/population/text()), \"-\")"
					);
			
			
			
			
			System.out.println(result);
		} catch (XQException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
