import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQExpression;
import javax.xml.xquery.XQResultSequence;

import org.jdom2.JDOMException;
import net.xqj.basex.BaseXXQDataSource;


public class BaseXImporter {

	XQDataSource ds = null;
	
	
	public BaseXImporter () throws XQException {

	}
	
	public String executeXQuery(String query) throws XQException, JDOMException, IOException {
		
		XQDataSource ds = new BaseXXQDataSource(); 
		ds.setProperty("serverName", "localhost");
		ds.setProperty("port", "1984");
		ds.setProperty("user", "admin");
		ds.setProperty("password", "admin");
		
		List<Continent> cons = new ArrayList<Continent>();
		List<Country> coun = new ArrayList<Country>();
		List<City> cit = new ArrayList<City>();
		
		String queryResult = "";
		//1. open connection		
		XQConnection basex = ds.getConnection();
		
		//2. create and execute XQuery
		XQExpression xquery = basex.createExpression();
		
		String continents = 
				"for $continent in db:open(\"factbook\",\"factbook.xml\")//continent "+
				"return string-join(($continent/@id, $continent/@name), \";\")";
	
		
		XQResultSequence results = xquery.executeQuery(continents); 
		//3. analyse result
		
		
		
		while(results.next()) {
			String continent = results.getItemAsString(null);
			String[] t = continent.split(";");
			if (!continent.equals("")) {
				cons.add(new Continent(t[0], t[1]));
			}
			
		}
		
		
		String countries = 
				"for $country in db:open(\"factbook\",\"factbook.xml\")//country "+
				"return string-join(($country/@id, $country/@name, $country/@population, $country/@capital), \";\")";
		
		results = xquery.executeQuery(countries); 
		//3. analyse result
		while(results.next()) {
			String country = results.getItemAsString(null);
			String[] t = country.split(";");
			if (!country.equals("")) {
				coun.add(new Country(t[0], t[1], t[3], Integer.parseInt(t[2]),"","","",""));
			}
			
		}
		
		
		String cities = 
				"for $country in db:open(\"factbook\",\"factbook.xml\")//country, $city in $country//city "+
				"return string-join(($city/@id, $city/name[1]/text(), $city/population/text(), $country/@id), \";\")";
		
		results = xquery.executeQuery(cities); 
		//3. analyse result
		while(results.next()) {
			String city = results.getItemAsString(null);
			System.out.println(city);
			String[] t = city.split(";");
			if (!city.equals("")) {
				cit.add(new City(t[0], t[1],t.length==3 ? -1 : Integer.parseInt(t[2]),t.length==3 ? t[2] : t[3]));
			}
			
		}
		
		for (Country co : coun) {
			
			String eth = 
					"for $country in db:open(\"factbook\",\"factbook.xml\")//country "+
					"where $country/@id = '"+ co.id +"' let $ethnic := $country/ethnicgroups "+
					"return $ethnic";
			
			results = xquery.executeQuery(eth); 
			//3. analyse result
			while(results.next()) {
				co.ethnics += results.getItemAsString(null);				
			}
			
			String bord = 
					"for $country in db:open(\"factbook\",\"factbook.xml\")//country "+
					"where $country/@id = '"+ co.id +"' let $ethnic := $country/border "+
					"return $ethnic";
			
			results = xquery.executeQuery(bord); 
			//3. analyse result
			while(results.next()) {
				co.border += results.getItemAsString(null);				
			}
			
			String rel = 
					"for $country in db:open(\"factbook\",\"factbook.xml\")//country "+
					"where $country/@id = '"+ co.id +"' let $ethnic := $country/religions "+
					"return $ethnic";
			
			results = xquery.executeQuery(rel); 
			//3. analyse result
			while(results.next()) {
				co.religions += results.getItemAsString(null);				
			}
			
			String lang = 
					"for $country in db:open(\"factbook\",\"factbook.xml\")//country "+
					"where $country/@id = '"+ co.id +"' let $l := $country/languages "+
					"return $l";
			
			results = xquery.executeQuery(lang); 
			//3. analyse result
			while(results.next()) {
				co.languages += results.getItemAsString(null);				
			}
			
			
		}
		
		
		
		String db = "USE [MedInf]\nGO\n\n";

		String continent = "";
		FileWriter out = new FileWriter(new File("Factbook_Continent_Inserts.txt"));
		for (Continent c : cons) {
			continent = "INSERT INTO [gnagj1].[Continent] " + "([ContinentId] "
					+ ",[ContinentName]) " + "VALUES "
					+ "('" + c.id + "' "
					+ ",'" + c.name+"')" + "\n\n";
			out.write(continent);
		}

		out.close();

		String country = "";
		out = new FileWriter(new File("Factbook_Country_Inserts.txt"));
		for (Country c : coun) {
			country = "INSERT INTO [gnagj1].[Country] " + "([CountryId] "
					+ ",[Name] " + ",[Capital] " + ",[PopulationCount] "
					+ ",[XmlData]) " + " VALUES "
					+ "('" + c.id
					+ "','" + c.name + "'"
					+ ",'" + c.capital
					+ "'," + c.population + ",'" + c.religions + c.border + c.ethnics + c.languages + "') " + "\n\n";
			out.write(country);
		}
		out.close();

		String city ="";
		out = new FileWriter(new File("Factbook_City_Inserts.txt"));
		for (City c : cit) {
			city = "INSERT INTO [gnagj1].[City] " + "([CityId] "
					+ ",[CityName] " + ",[CountryId],[PopulationCount]) " + "VALUES "
					+ "('" + c.id
					+ "','" + c.name
					+ "','" + c.country
					+ "'," + c.population + " ) " + "\n\n";
			out.write( city);
		}

//		String ContinentCountry;
//		for (Country c : coun) {
//			
//			for (Continent co : cons) {
//				
//				if (c.continent == co.id) {
//					ContinentCountry = "INSERT INTO [gnagj1].[ContinentCountry] "
//							+ " ([ContinentId] "
//							+ ",[CountryId]) "
//							+ " VALUES "
//							+ "(<ContinentId, varchar(20),> "
//							+ ",<CountryId, varchar(20),>) " + "\n";
//				}
//			}
//		}
		
		
		
		
		
		out.close();
		
		
		cit.size();
		coun.size();
		cons.size();
		
		//4. close connection
		basex.close();
		
		
		return queryResult;
	}
	
	public List<Continent> getContinents(String query) throws XQException {
		List<Continent> co = new ArrayList<>();
		
		//1. open connection		
		XQConnection basex = ds.getConnection();
		//2. create and execute XQuery
		XQExpression xquery = basex.createExpression();
		XQResultSequence results = xquery.executeQuery(query); 
		//3. analyse result
		while(results.next()) {
				
		}
		//4. close connection
		basex.close();
		
		
		return co;
	}
	

}
